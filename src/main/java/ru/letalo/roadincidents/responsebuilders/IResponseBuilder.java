/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.letalo.roadincidents.responsebuilders;

/**
 *
 * @author gerasimov.pk
 */
public interface IResponseBuilder {
    public String build (Object o);
}
