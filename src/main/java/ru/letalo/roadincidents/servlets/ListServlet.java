/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.letalo.roadincidents.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;

/**
 *
 * @author gerasimov.pk
 */
public class ListServlet extends AbstractCustomServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        super.doGet(req, resp);
        showXML("<list>"
                + "     <request>"
                + "         <address>��.������� ���������, 198</address>"
                + "         <lat>56.23672</lat>"
                + "         <lon>40.34672</lon>"
                + "         <lon>40.34672</lon>"
                + "         <status>1</status>"
                + "     </request>"
                + "     <request>"
                + "         <address>��.������� ���������, 198</address>"
                + "         <lat>56.23672</lat>"
                + "         <lon>40.34672</lon>"
                + "         <lon>40.34672</lon>"
                + "         <status>1</status>"
                + "     </request>"
                + "     <request>"
                + "         <address>��.������� ���������, 198</address>"
                + "         <lat>56.23672</lat>"
                + "         <lon>40.34672</lon>"
                + "         <lon>40.34672</lon>"
                + "         <status>1</status>"
                + "     </request>"
                + "</list>");
    }

}
