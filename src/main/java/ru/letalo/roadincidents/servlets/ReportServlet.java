/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.letalo.roadincidents.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;

/**
 *
 * @author gerasimov.pk
 */
public class ReportServlet extends AbstractCustomServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        super.doGet(req, resp);
        String id = req.getParameter("id");
        showXML("<report>"
                + "     <drone id='1' name='sample drone' model='quadrocopter'/>"
                + "     <lon>40.23561</lon>"
                + "     <lat>56.13598</lat>"
                + "</report>");
    }

}
