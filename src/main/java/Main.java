import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.Random;
import ru.letalo.roadincidents.servlets.*;

public class Main extends HttpServlet {





  public static void main(String[] args) throws Exception{
    Server server = new Server(Integer.valueOf(System.getenv("PORT")));
    ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
    context.setContextPath("/");
    server.setHandler(context);
    context.addServlet(new ServletHolder(new LoginServlet()),"/login");
    context.addServlet(new ServletHolder(new RegisterServlet()),"/register");
    context.addServlet(new ServletHolder(new LogoutServlet()),"/logout");
    context.addServlet(new ServletHolder(new ReportServlet()),"/report");
    context.addServlet(new ServletHolder(new RequestServlet()),"/request");
    context.addServlet(new ServletHolder(new ListServlet()),"/list");
    server.start();
    server.join();
  }
}
